# README #
#
Author: Pablo González Martínez
#
pablogonzmartinez@gmail.com

Unity3D Engine
C#

This project  showcases the implementation of flocking algorithms.
It allows the user to create agents that will move around and steer based on flocking and collision avoidance behaviour.

# Google Docs doc #
https://docs.google.com/document/d/1lvGIqKpIlU8xoDd3ACRp0hULLxh0iapWsc9TmI8D-UI/edit?usp=sharing

# Requirements #
The project has been created with Unity 2017.1.1f1 (64-bit).

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant. It should be opened in Unity using the parent folder containing ‘Assets’ and ‘Project Settings’ folders.

The project is organized in folders. The main scene is labelled as ‘test’, and is contained in the Scenes folder.

All the code is contained in the Scripts folder and has been created exclusively by the author.
