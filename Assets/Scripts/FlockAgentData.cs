
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Flock Agent", menuName = "Flock Agent")]
public class FlockAgentData : AgentData {
    
    [SerializeField]
    private float collisionAvoidanceRange = 0.75f;
    public  float CollisionAvoidanceRange { get { return collisionAvoidanceRange; } }

    [SerializeField]
    private float alignmentRange = 2;
    public  float AlignmentRange { get { return alignmentRange; } }

    [SerializeField]
    private float cohesionSeparationRange;
    public  float CohesionSeparationRange { get { return cohesionSeparationRange; } }
    
    [SerializeField]
    private float visionAngle = 180;
    public  float VisionAngle { get { return visionAngle; } }
}
