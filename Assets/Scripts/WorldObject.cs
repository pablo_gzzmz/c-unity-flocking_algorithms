
using System.Collections.Generic;
using UnityEngine;

public abstract class WorldObject : MonoBehaviour {
    
    private static Dictionary<int, WorldObject> all = new Dictionary<int, WorldObject>();
    public  static Dictionary<int, WorldObject> All { get { return all; } }

    private int id;
    public  int ID { get { return id; } }

    [Space(12)]
    [SerializeField]
    protected float radius = 0.5f;
    public    float Radius { get { return radius; } }

    public abstract bool participatesInFlocking { get; }
    public abstract bool isCollider      { get; }
    public abstract bool shouldBeIgnored { get; }

    protected virtual void Awake() {
        id = GetInstanceID();

        all.Add(id, this);
    }

    protected virtual void OnDestroy() {
        all.Remove(id);
    }
}
