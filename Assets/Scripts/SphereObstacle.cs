
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereObstacle : WorldObject {

    public override bool participatesInFlocking { get { return false; } }
    public override bool isCollider      { get { return true; } }
    public override bool shouldBeIgnored { get { return false; } }

    protected override void Awake() {
        base.Awake();
    }

    public void SetRadius(float _radius) {
        radius = _radius;

        float value = radius * 2;
        transform.localScale = new Vector3(value, value, value); 
    }
}
