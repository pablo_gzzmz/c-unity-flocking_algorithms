
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Agent<T> : WorldObject where T : AgentData {

    [SerializeField]
    protected T data;

    protected AgentState state = AgentState.Alive;
    public    AgentState State { get { return state; } }

    protected bool paused;
    public    bool Paused { get { return paused; } set { paused = value; } }

    private new SphereCollider collider;

    private new Renderer renderer;
    public      Renderer Renderer { get { return renderer; } }

    private Color originalColor;
    public  Color OriginalColor { get { return originalColor; } }
    private Color currentColor;
    public  Color CurrentColor {
        get { return currentColor; }
        set { renderer.material.color = value; currentColor = value; } }

    public override bool shouldBeIgnored { get { return state != AgentState.Alive; } }

    protected bool moving;
    protected int  steering;

    private void OnValidate() {
        if (collider != null) collider.radius = radius;
    }

    protected virtual void Start() {
        collider = GetComponentInChildren<SphereCollider>();
        collider.radius = radius;

        renderer = GetComponentInChildren<Renderer>();
        originalColor = renderer.material.color;
        CurrentColor = originalColor;

        moving = true;
    }

    private void Update() {
        if (paused) return;

        CalculateSteering();
        Steer();
        Move();
    }

    protected virtual void CalculateSteering() {}

    /// <summary>
    /// Performs a basic rotation steering based on current steering force.
    /// </summary>
    protected virtual void Steer() {
        if (steering == -1) {
            // To the left
            transform.rotation *= Quaternion.AngleAxis(-data.SteeringForce * Time.deltaTime, Vector3.up);
        }
        else
        if (steering ==  1) {
            // To the right
            transform.rotation *= Quaternion.AngleAxis( data.SteeringForce * Time.deltaTime, Vector3.up);
        }
    }

    /// <summary>
    /// Performs basic forward movement.
    /// </summary>
    protected virtual void Move() {
        if (moving) {
            transform.position += transform.forward * data.Speed * Time.deltaTime;

            OutsideWindowFix();
        }
    }

    /// <summary>
    /// Fixes the position if outside window boundaries, making agents appear at the other side
    /// </summary>
    protected void OutsideWindowFix() {
        Vector3 position = transform.position;

        // Outside right boundary
        if (position.x - radius > ScreenHelper.rightBoundary) {
            position.x = ScreenHelper.leftBoundary - radius;
            transform.position = position;
        }
        else
        if (position.x + radius < ScreenHelper.leftBoundary) {
            position.x = ScreenHelper.rightBoundary + radius;
            transform.position = position;
        }

        // Outside top/bottom boundary
        if (position.z - radius > ScreenHelper.topBoundary) {
            position.z = ScreenHelper.botBoundary - radius;
            transform.position = position;
        }
        else
        if (position.z + radius < ScreenHelper.botBoundary) {
            position.z = ScreenHelper.topBoundary + radius;
            transform.position = position;
        }
    }
}
