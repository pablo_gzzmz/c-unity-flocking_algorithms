
public enum AgentState {
    Alive,
    Dead,
    Escaped
}

public enum VisionType {
    Wide,
    Limited,
    Narrow
}