
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple helper to establish world boundaries based on camera
/// </summary>

public class ScreenHelper : MonoBehaviour {

    private static float distance;

    private static float[] xBoundaries = new float[2];
    private static float[] yBoundaries = new float[2];
    
    public static float leftBoundary  { get { return xBoundaries[0]; } }
    public static float rightBoundary { get { return xBoundaries[1]; } }
    public static float topBoundary   { get { return yBoundaries[1]; } }
    public static float botBoundary   { get { return yBoundaries[0]; } }

    private new static Camera camera;

    private void Reset() {
        Debug.Assert(GetComponent<Camera>() != null);
    }

    private void Awake () {
        camera = GetComponent<Camera>();
        Debug.Assert(camera != null);

        distance = transform.position.y;

        Vector3 topLeft  = camera.ViewportToWorldPoint(new Vector3(0, 1, distance));
        Vector3 botRight = camera.ViewportToWorldPoint(new Vector3(1, 0, distance));

        xBoundaries[0] = topLeft.x;
        xBoundaries[1] = botRight.x;
        yBoundaries[0] = botRight.z;
        yBoundaries[1] = topLeft.z;
	}
}
