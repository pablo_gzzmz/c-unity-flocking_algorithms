
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component used for basic testing.
/// </summary>

public class Tester : MonoBehaviour {

    private Collider lastHit;

    private FlockAgent hovered;

    private bool hovering;
    private bool gamePaused;

    private LayerMask groundMask;

    private void Awake() {
        groundMask = LayerMask.NameToLayer("Ground");
        groundMask = 1 << groundMask;
    }

    private void Update() {
        PauseInput();

        LookForHoveredAgent();

        if (hovered != null) {
            HoveredAgentOptions();
        }
        else if(!hovering) {
            SpawnOptions();
        }
    }

    private void LookForHoveredAgent() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1)) {
            if (hit.collider != lastHit) {
                if (hovered != null) {
                    hovered.Renderer.material.color = hovered.CurrentColor;
                }

                hovered = hit.collider.transform.root.GetComponent<FlockAgent>();

                if (hovered != null) {
                    hovered.Renderer.material.color = Color.white;
                }

                lastHit = hit.collider;
                hovering = true;
            }
        }

        else {
            if (hovered != null) {
                hovered.Renderer.material.color = hovered.CurrentColor;
            }

            lastHit = null;
            hovered = null;
            hovering = false;
        }
    }

    private void HoveredAgentOptions() {
        // Force rotation
        if (Input.GetKeyDown(KeyCode.W)) {
            hovered.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else
        if (Input.GetKeyDown(KeyCode.S)) {
            hovered.transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if (Input.GetKeyDown(KeyCode.A)) {
            hovered.transform.eulerAngles = new Vector3(0, -90, 0);
        }
        else
        if (Input.GetKeyDown(KeyCode.D)) {
            hovered.transform.eulerAngles = new Vector3(0, 90, 0);
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            hovered.IsLeader = true;
        }

        // Pause / Resume on left click
        if (Input.GetMouseButtonDown(0)) {
            hovered.Paused = !hovered.Paused;
        }

        // Destroy on right click
        if (Input.GetMouseButtonDown(1)) {
            Destroy(hovered.gameObject);
        }
    }

    private void PauseInput() {
        if (Input.GetKeyDown(KeyCode.P)) {
            gamePaused = !gamePaused;

            if (gamePaused) {
                Time.timeScale = 0;
            }
            else {
                Time.timeScale = 1;
            }
        }
    }

    private void SpawnOptions() {
        if (Input.GetMouseButtonDown(1)) {
            Instantiate(Resources.Load("Prefabs/Agent"), GetMousePosition(), Quaternion.identity);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            SpawnObstacle(0.15f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            SpawnObstacle(0.3f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            SpawnObstacle(0.5f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            SpawnObstacle(0.75f);
        }
    }

    private void SpawnObstacle(float radius) {
            GameObject go = Instantiate(Resources.Load("Prefabs/Obstacle"), GetMousePosition(), Quaternion.identity) as GameObject;

            go.GetComponent<SphereObstacle>().SetRadius(radius);
    }

    private Vector3 GetMousePosition() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, groundMask)) {
            return hit.point;
        }

        else return -Vector3.one;
    }
}
