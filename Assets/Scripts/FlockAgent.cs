
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockAgent : Agent<FlockAgentData> {

    private static FlockingProfile profile;

    public override bool isCollider { get { return false; } }
    public override bool participatesInFlocking { get { return !isLeader; } }
    
    private bool isLeader;
    public  bool IsLeader {
        get { return isLeader; }
        set {
            isLeader = value;
            CurrentColor = isLeader ? Color.green : OriginalColor;
        }
    }

    private float visionRange;
    private float flockingRange;

    private float halfVisionAngle;
    private float collisionAvoidanceRangeMultiplier;

    public const float frontAreaThreshold = 0.05f;

    // Resulting flocking steer that will be used to calculate in which direction to go
    private float totalSteer;

    protected virtual void OnValidate() {
        SetVision();
    }

    protected override void Awake() {
        base.Awake();

        if(profile == null) profile = Resources.Load<FlockingProfile>("Flocking Profile");

        SetVision();
    }

    private void SetVision() {
        halfVisionAngle = data.VisionAngle * 0.5f;
        visionRange   = Mathf.Max(new float[3] {data.CollisionAvoidanceRange, data.AlignmentRange, data.CohesionSeparationRange });
        flockingRange = Mathf.Max(data.AlignmentRange, data.CohesionSeparationRange);

        collisionAvoidanceRangeMultiplier = 1 / data.CollisionAvoidanceRange;
    }

    protected override void CalculateSteering() {
        Flock();
    }

    /// <summary>
    /// Perform calculations on separation, cohesion, alignment and collision detection to determine the current steering.
    /// </summary>
    private void Flock() {
        Vector3 averagePosition  = Vector3.zero;
        Vector3 averageDirection = Vector3.zero;

        totalSteer = 0;

        int nearbyCount = 0;
        bool avoidingCollision = false;

        foreach (WorldObject wObject in All.Values) {
            if (wObject.ID == ID || wObject.shouldBeIgnored) continue;

            Vector3 towards = wObject.transform.position - transform.position;

            // Outisde of vision range check
            float distance = Mathf.Abs(towards.magnitude) - wObject.Radius - radius;
            if (distance > visionRange) continue;

            // Angle check
            float angle = Vector3.Angle(transform.forward, towards.normalized);
            if (angle > halfVisionAngle) continue;

            float dot = Vector3.Dot(Vector3.Cross(transform.forward, towards.normalized), Vector3.up);
            // Get left/right orientation
            float orientation = dot > 0f ? 1f : -1f;

            if (distance <= data.CollisionAvoidanceRange) {
                if (!isLeader || wObject.isCollider) {
                    CalculateCollisionAvoidance(wObject, distance, orientation);
                    avoidingCollision = true;
                }
            }

            if (wObject.participatesInFlocking) {
                averagePosition  += wObject.transform.position;
                averageDirection += wObject.transform.forward;
                nearbyCount++;
            }
        }

        if (!avoidingCollision && !isLeader && nearbyCount > 0) {
            averagePosition  /= nearbyCount;
            averageDirection /= nearbyCount;
            averageDirection.y = 0;
            averageDirection.Normalize();

            Vector3 towards = averagePosition - transform.position;
            float distance = Mathf.Abs(towards.magnitude) - radius;

            if (distance <= flockingRange) {
                float dot = Vector3.Dot(Vector3.Cross(transform.forward, towards.normalized), Vector3.up);
                float orientation = dot > 0f ? 1f : -1f;

                if (distance <= data.CohesionSeparationRange)
                    CalculateCohesionAndSeparation(orientation, distance);
                
                if(distance <= data.AlignmentRange) 
                    CalculateAlignment(averageDirection);
            }
        }

        if (totalSteer >  frontAreaThreshold) steering =  1; // Right
        else
        if (totalSteer < -frontAreaThreshold) steering = -1; // Left
        else steering = 0;
    }

    /// <summary>
    /// Calculate the adecuate steer based on collision avoidance per object and add to the total steer.
    /// </summary>
    private void CalculateCollisionAvoidance(WorldObject wObject, float distance, float orientation) {
        float t = distance * collisionAvoidanceRangeMultiplier; // = distance / collisionAvoidanceRange
        float steer = Mathf.Lerp(0, 1, t);
        steer = Mathf.Pow(10, steer);
        steer *= -orientation;

        totalSteer += steer;
    }

    /// <summary>
    /// Calculate the adecuate steer based on cohesion and separation towards an average position and add to the total steer.
    /// </summary>
    private void CalculateCohesionAndSeparation(float orientation, float distance) {
        float t = distance / data.CohesionSeparationRange;
        float steer = Mathf.Lerp(-profile.separationToCohesionFactor, 1, t) * orientation;

        totalSteer += steer * profile.separationCohesionMultiplier;
    }

    /// <summary>
    /// Calculate the adecuate steer based on alignment towards an average direction and add to the total steer.
    /// </summary>
    private void CalculateAlignment(Vector3 averageDirection) {
        float dot = Vector3.Dot(averageDirection, transform.forward);

        float orientation = Vector3.Dot(Vector3.Cross(transform.forward, averageDirection), Vector3.up);
        orientation = orientation > 0f ? 1f : -1f;

        if (dot < 0.97f) {
            totalSteer += orientation * profile.alignmentMultiplier * Mathf.Abs(dot - 1.0f);
        }
    }
}
