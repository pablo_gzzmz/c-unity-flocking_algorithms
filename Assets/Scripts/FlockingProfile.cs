
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Flocking Profile", menuName = "Flocking Profile")]
public class FlockingProfile : ScriptableObject {

    public float separationToCohesionFactor = 3f;
    public float separationCohesionMultiplier = 5f;
    public float alignmentMultiplier = 0.05f;

}
