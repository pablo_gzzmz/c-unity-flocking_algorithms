
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AgentData : ScriptableObject {

    
    [SerializeField]
    private float speed;
    public  float Speed { get { return speed; } }

    [SerializeField]
    private float steeringForce = 50f;
    public  float SteeringForce { get { return steeringForce; } }
}
